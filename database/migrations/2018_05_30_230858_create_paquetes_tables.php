<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaquetesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paquetes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('noCambio')->nullable()->default(null);
            $table->timestamp('created')->useCurrent();
            $table->string('nombre')->nullable()->default(null);
            $table->string('descripcion')->nullable()->default(null);
            $table->string('tiempo')->nullable()->default(null);
            $table->double('precio',17,2)->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('app')->unsigned()->nullable()->default(null);
            $table->foreign('app')->references('id')->on('aplicaciones')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paquetes');
    }
}
