<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCambiosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cambios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('noCambio')->nullable()->default(null);
            $table->timestamp('created')->useCurrent();
            $table->string('ern')->nullable()->default(null);
            $table->string('token')->nullable()->default(null);
            $table->string('aprobacion')->nullable()->default(null);
            $table->string('fechaapro')->nullable()->default(null);
            $table->string('comentario')->nullable()->default(null);
            $table->date('fechaInicio')->nullable()->default(null);
            $table->date('fechaFin')->nullable()->default(null);
            $table->tinyInteger('estado')->nullable()->default(1);

            $table->integer('cliente')->unsigned()->nullable()->default(null);
            $table->foreign('cliente')->references('id')->on('clientes')->onDelete('cascade');

            $table->integer('ventasdetalle')->unsigned()->nullable()->default(null);
            $table->foreign('ventasdetalle')->references('id')->on('ventasdetalle')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cambios');
    }
}
